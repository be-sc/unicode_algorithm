*****************
unicode_algorithm
*****************

Literature
##########

* `Wikipedia: Unicode <https://en.wikipedia.org/wiki/Unicode>`_
* `Unicode Standard Overview <https://www.unicode.org/versions/latest/>`_
* `Unicode Core Specification <https://www.unicode.org/versions/latest/#Core_Specification>`_
* `Unicode Standard Annexes (UAX) <https://www.unicode.org/versions/latest/#UAX_Changes>`_
* `UAX #29: Unicode Text Segmentation <https://unicode.org/reports/tr29/>`_
* `Character Database <https://www.unicode.org/Public/UCD/latest/>`_
