#[============================================================[
:Author: https://bitbucket.org/be-sc/ppboil
:Version: 2.0.1
:Copyright: CC0/public domain

Provides functions for project and target configuration.
#]============================================================]


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#[=[.rst:
C++ and C compiler detection. This means:

* Detailed detection for the “big 3”: Clang, GCC, MSVC.
* Compilers that openly simulate GCC or MSVC via the ``CMAKE_<lang>_SIMULATE_ID`` variable are
  recognized as GCC compatible or MSVC compatible.
* Also recognizes the Intel compiler as GCC compatible on non-Windows platforms.

Output variables for C++:

``CXX_GCC_COMPAT``
    True if the compiler is GCC, or Clang in GCC-compatible command line mode, or another compiler
    reported as GCC compatible.
``CXX_GCC``
    True if the compiler is the real GCC.
``CXX_CLANG``
    True if the compiler is Clang, no matter in which mode.
``CXX_MSVC``
    True if the compiler is the real MSVC.
``CXX_MSVC_COMPAT``
    True if the compiler is MSVC, or Clang in MSVC-compatible command line mode (clang-cl), or
    another compiler reported as MSVC compatible.
``CXX_COMPILER_DETECTED``
    True if any C++ compiler was detected, no matter which one.

For C the same set of variables is defined with the prefix ``C_`` instead of ``CXX__``.

Calling this function multiple times is valid. For an already detected compiler the detection does
not run again. Its variables remain unchanged.
#]=]
function(detect_cxx_and_c_compilers)
    if (NOT DEFINED PROJECT_NAME)
        strong(TOLOWER ${lang} lang_lower)
        message(FATAL_ERROR "detect_cxx_and_c_compiler() must be called after project().")
    endif()

    if (CMAKE_CXX_COMPILER_ID)
        ppboil_detect_c_or_cxx_compiler(CXX)
    endif()

    if (CMAKE_C_COMPILER_ID)
        ppboil_detect_c_or_cxx_compiler(C)
    endif()
endfunction()


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#[=[.rst:
Checks if a C++ compiler was detected with ``detect_cxx_and_c_compilers()`` and aborts with a
``FATAL_ERROR`` if not. May be called with a target name as parameter, which appears in the error
message.
#]=]
macro(check_cxx_compiler_detected)
    if (NOT CXX_COMPILER_DETECTED)
        if (ARGC EQUAL 0)
            message(FATAL_ERROR "Unclear C++ compiler. Call detect_cxx_and_c_compilers() first.")
        else()
            message(
                FATAL_ERROR
                "Unclear C++ compiler. Call detect_cxx_and_c_compilers() first. [target: ${ARGV0}]"
            )
        endif()
    endif()
endmacro()


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#[=[.rst:
Checks if a C compiler was detected with ``detect_cxx_and_c_compilers()`` and aborts with a
``FATAL_ERROR`` if not. May be called with a target name as parameter, which appears in the error
message.
#]=]
macro(check_c_compiler_detected)
    if (NOT C_COMPILER_DETECTED)
        if (ARGC EQUAL 0)
            message(FATAL_ERROR "Unclear C compiler. Call detect_cxx_and_c_compilers() first.")
        else()
            message(
                FATAL_ERROR
                "Unclear C compiler. Call detect_cxx_and_c_compilers() first. [target: ${ARGV0}]"
            )
        endif()
    endif()
endmacro()


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#[=[.rst:
Sets the publicly required C++ standard version for a target. Does not allow fallback to an earlier
standard. Accepted values for ``version`` are the same as for CMake’s ``CXX_STANDARD`` variable.
Specify ``EXTENSIONS`` as the third argument if you want to allow compiler extensions.
#]=]
function(target_cxx_standard target_name version)
    check_cxx_compiler_detected(${target_name})

    if (ARGC GREATER 2)
        if (NOT "${ARGV2}" STREQUAL "EXTENSIONS")
            message(FATAL_ERROR "Unknown option: ${ARGV2} [target: ${target_name}]")
        endif()
        set(allow_extensions ON)
    else()
        set(allow_extensions OFF)
    endif()

    set_target_properties(${target_name} PROPERTIES
        CXX_STANDARD ${version}
        CXX_STANDARD_REQUIRED ON
        CXX_EXTENSIONS ${allow_extensions}
    )

    if (NOT CMAKE_VERSION VERSION_LESS "3.8")
        target_compile_features(${target_name} PUBLIC cxx_std_${version})
    endif()

    if (CXX_MSVC)
        # MSVC special logic does two things. 1) It mimics CXX_EXTENSIONS as far as possible.
        # 2) It ensures support for the alternative operator names defined in [lex.digraph]
        # in the standard – e.g. *not* instead of *!*. (1) is only possible in a useful way with
        # the /permissive- switch available since Visual Studio 2017 (compiler version 19.10).
        # (2) can be achieved either through /permissive- or through force-including the iso646.h
        # header.
        if (NOT allow_extensions AND ${MSVC_VERSION} GREATER_EQUAL 1910)
            target_compile_options(${target_name} PUBLIC /permissive-)
        else()
            target_compile_options(${target_name} PUBLIC /FIiso646.h)
        endif()
    endif()
endfunction()


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#[=[.rst:
Sets the publicly required C++ standard version for a target. Does not allow fallback to an earlier
standard. Accepted values for ``version`` are the same as for CMake’s ``C_STANDARD`` variable.
Specify ``EXTENSIONS`` as the third argument if you want to allow compiler extensions.
#]=]
function(target_c_standard target_name version)
    check_c_compiler_detected(${target_name})

    if (ARGC GREATER 2)
        if (NOT "${ARGV2}" STREQUAL "EXTENSIONS")
            message(FATAL_ERROR "Unknown option: ${ARGV2} [target: ${target_name}]")
        endif()
        set(allow_extensions ON)
    else()
        set(allow_extensions OFF)
    endif()

    set_target_properties(${target_name} PROPERTIES
        C_STANDARD ${version}
        C_STANDARD_REQUIRED ON
        C_EXTENSIONS ${allow_extensions}
    )

    if (NOT CMAKE_VERSION VERSION_LESS "3.8")
        target_compile_features(${target_name} PUBLIC c_std_${version})
    endif()
endfunction()


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#[=[.rst:
Safely appends link flags to the existing link flags of the given target. Because the ``LINK_FLAGS``
property is a space separated string combined with CMake’s rules about spaces and semicolons
appending by hand is awkward and error prone.

Usage::

    target_link_flags(target_name <flag1> ...)
#]=]
if (CMAKE_VERSION VERSION_LESS "3.12")
    function(target_link_flags target_name)
        # LINK_FLAGS is a space separated string. Joining and quoting as below is mandatory.
        # loop instead of string(REPLACE) to handle values with semicolons correctly
        foreach(value ${ARGN})
            string(APPEND flags " ${value}")
        endforeach()

        set_property(TARGET ${target_name} APPEND_STRING PROPERTY LINK_FLAGS " ${flags}")
    endfunction()
else()
    function(target_link_flags target_name)
        list(JOIN ARGN " " flags)
        set_property(TARGET ${target_name} APPEND_STRING PROPERTY LINK_FLAGS " ${flags}")
    endfunction()
endif()


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#[=[.rst:
Sets the symbol visibility for shared libraries. Has no effect on other types of targets.

If you want to create a header file too containing the usual export/API macros, consider using
``configure_library_api()`` from the ``LibraryApi`` module instead.
#]=]
function(target_lib_visibility target_name visi inline_visi)
    set_target_properties(${target_name} PROPERTIES
        CXX_VISIBILITY_PRESET ${visi}
        VISIBILITY_INLINES_HIDDEN ${inline_visi}
    )
endfunction()


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#[=[.rst:
Enables strict ``PRIVATE`` C++ compiler warnings and turns some warnings into errors.
If you aim at clean, modern C++ this or something similar is probably what you want.
#]=]
function(enable_strict_cxx_checks target_name)
    check_cxx_compiler_detected(${target_name})

    if (CXX_GCC_COMPAT)
        target_compile_options(${target_name} PRIVATE
            -Wall -Wextra -Wpedantic
            -Wold-style-cast     # C-style cast, e.g. (int)variable
            -Werror=return-type  # missing return type or missing return in non-void function
            -Werror=reorder      # member order mismatch between class and ctor init list
        )

        if (CXX_GCC)
            target_compile_options(${target_name} PRIVATE -Werror=suggest-override)
        elseif (CXX_CLANG)
            target_compile_options(${target_name} PRIVATE -Werror=inconsistent-missing-override)
        endif()

    elseif (CXX_MSVC_COMPAT)
        target_compile_options(${target_name} PRIVATE
            /W3
            /w34053  # one void operand for '?:'
            /w34062  # enumerator 'identifier' in switch of enum 'enumeration' is not handled
            /w34100  # unreferenced formal parameter
            /w34127  # conditional expression is constant
            /w34189  # local variable is initialized but not referenced
            /w34296  # 'operator' : expression is always false
            /w34471  # forward declaration of an unscoped enumeration must have an underlying type
        )
    endif()
endfunction()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# implementation details


macro(ppboil_detect_c_or_cxx_compiler lang)
    if (NOT ${lang}_COMPILER_DETECTED)
        set(${lang}_COMPILER_DETECTED TRUE PARENT_SCOPE)
        set(${lang}_GCC_COMPAT FALSE PARENT_SCOPE)
        set(${lang}_GCC FALSE PARENT_SCOPE)
        set(${lang}_CLANG FALSE PARENT_SCOPE)
        set(${lang}_MSVC_COMPAT FALSE PARENT_SCOPE)
        set(${lang}_MSVC FALSE PARENT_SCOPE)

        if (CMAKE_${lang}_COMPILER_ID STREQUAL "GNU")
            set(${lang}_GCC TRUE PARENT_SCOPE)
            set(${lang}_GCC_COMPAT TRUE PARENT_SCOPE)

        elseif (CMAKE_${lang}_COMPILER_ID MATCHES "Clang$") # need fuzzy match to catch Apple Clang
            set(${lang}_CLANG TRUE PARENT_SCOPE)

            if (CMAKE_${lang}_SIMULATE_ID STREQUAL "MSVC")  # catch clang-cl
                set(${lang}_MSVC_COMPAT TRUE PARENT_SCOPE)
            else()
                set(${lang}_GCC_COMPAT TRUE PARENT_SCOPE)
            endif()

        elseif (CMAKE_${lang}_COMPILER_ID STREQUAL "MSVC")
            set(${lang}_MSVC_COMPAT TRUE PARENT_SCOPE)
            set(${lang}_MSVC TRUE PARENT_SCOPE)

        elseif (NOT WIN32 AND CMAKE_${lang}_COMPILER_ID STREQUAL "Intel")
            set(${lang}_GCC_COMPAT TRUE PARENT_SCOPE)
        endif()


        if (CMAKE_${lang}_SIMULATE_ID STREQUAL "GNU")
            set(${lang}_GCC_COMPAT TRUE PARENT_SCOPE)
        elseif (CMAKE_${lang}_SIMULATE_ID STREQUAL "MSVC")
            set(${lang}_MSVC_COMPAT TRUE PARENT_SCOPE)
        endif()
    endif()
endmacro()
