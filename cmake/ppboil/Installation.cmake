#[============================================================[
:Author: https://bitbucket.org/be-sc/ppboil
:Version: 2.1.1
:Copyright: CC0/public domain

Provides functionality to automate target installation and package export. CMake requires quite a
bit of work to get a full-featured installation working correctly. The central function in this
file – ``install_package()`` – abstracts a lot of the details away for the most common cases.

For the exported CMake package this module uses the common practice of creating a top-level
*<package>Config.cmake* file for custom code, that uses ``include()`` to pull in the main target
config file created by ``install(EXPORT)``, which is renamed to *<package>TargetConfig.cmake*.

A common piece of custom code is automating finding transitive dependency by placing the appropriate
``find_dependency()`` calls in the exported CMake package. The ``find_package_and_export()``
function in this module provides a way to record ``find_package()`` calls relevant for installation.
#]============================================================]

include(CMakePackageConfigHelpers)
set(ppboil_Installation_cmake_dir "${CMAKE_CURRENT_LIST_DIR}")

# The project using ppboil might not require semver support.
if (EXISTS "${ppboil_Installation_cmake_dir}/Versioning.cmake")
    include("${ppboil_Installation_cmake_dir}/Versioning.cmake")
    set(ppboil_Installation_cmake_default_version_compat SemanticVersion)

    macro(ppboil_write_version_file)
        write_package_version_file(${ARGN})
    endmacro()
else()
    set(ppboil_Installation_cmake_default_version_compat AnyNewerVersion)

    macro(ppboil_write_version_file)
        write_basic_package_version_file(${ARGN})
    endmacro()
endif()


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#[=[.rst:
Calls ``find_package()`` with the given arguments and records the call for later use in an exported
package.

The recored calls are stored per project in the variable ``<project-name>_FIND_DEPENDENCY_CALLS``.
The original arguments are preserved and the function name is changed to ``find_dependency()``.
Calls are separated by line breaks. When dumped into the *<package>Config.cmake* file of an exported
package the content of this variable can be executed as is to find transitive dependencies.
``install_package()`` from this module has support for processing the records from this functions.
#]=]
macro(find_package_and_export)
    find_package(${ARGN})

    # Replacing will fail with arguments that contain semicolons.
    # However, that should never be the case in a find_package() call.
    string(REPLACE ";" " " ppboil_argn_with_spaces "${ARGN}")
    string(APPEND ${PROJECT_NAME}_FIND_DEPENDENCY_CALLS
        "find_dependency("
        "${ppboil_argn_with_spaces}"
        ")\n"
    )
endmacro()


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#[=[.rst:
Installs a complete package: build artefacts, additional files like public headers, and an exported
CMake package including version info.

Usage::

    install_package(
        TARGETS <target1> ...
        [ # export configuration
          NO_EXPORT | [
            [ # version configuration
              NO_VERSION | [
                [VERSION {<major.minor.patch>|<semantic version>}]
                [COMPATIBILITY {<built-in specifier>|SemanticVersion}]
                [ARCH_INDEPENDENT]
              ]
            ]
            [EXPORT_PREFIX <prefix>]
            [NO_NAMESPACE]
            [CONFIG_TEMPLATE_FILE <file>]
            [FIND_DEPENDENCY_CALLS {<variable-name>|"<calls1>"} ...]
            [PUBLIC_INCLUDE_DIRS <dir1> ...]
          ]
        ]
        # destination directory configuration
        [ARCHIVE_DIR <dir>]
        [LIBRARY_DIR <dir>]
        [RUNTIME_DIR <dir>]
        [OBJECTS_DIR <dir>]
        [FRAMEWORK_DIR <dir>]
        [BUNDLE_DIR <dir>]
        [PUBLIC_HEADER_DIR <dir>]
        [PRIVATE_HEADER_DIR <dir>]
        [RESOURCE_DIR <dir>]
        [INCLUDE_DIR <dir>]
    )

``TARGETS`` is the list of targets to be installed, just like in the built-in ``install()`` command.

Parameters for configuring the exported CMake package:

``NO_EXPORT``
    Disables creation of the exported CMake package. Only build artefacts are installed. All other
    export/version config arguments are ignored.
``EXPORT_PREFIX``
    Defines the prefix, i.e. the name of the installed/exported package. Also used to assemble file
    names for the exported CMake project. Defaults to ``PROJECT_NAME``.
``NO_NAMESPACE``
    Does not put the exported targets into a namespace. Each target would normally be exported with
    a namespaced name according to the pattern ``<EXPORT_PREFIX>::<target-name>``. If this option
    is set a simple ``<target-name>`` is used.
``CONFIG_TEMPLATE_FILE``
    If present defines the path to a custom custom template file for generating the main exported
    package file *<package>Config.cmake*. The template is used with ``configure_file(@ONLY)``.
``FIND_DEPENDENCY_CALLS``
    The relevant calls to ``find_dependency()``. Each argument can either be a string containing
    calls separated by newlines or the name of a variable that contains such a string. Arguments
    can be a mixture of both variants. All arguments are concatenated without any glue characters.
    Defaults to the ``<project-name>_FIND_DEPENDENCY_CALLS`` variable generated by
    ``find_package_and_export()`` for the current project.
``PUBLIC_INCLUDE_DIRS``
    A list of directories containing public header files. They are installed as subdirectories of
    ``INCLUDE_DIR``. Directories with the same name are merged into one.

Parameters for configuring the version info file:

These parameters control if/how a *<package>ConfigVersion.cmake* file is created. Their behaviour
depends on whether ppboil’s *Versioning* module is present in the same directory as
*Installation*. If it is the parameters work the same as described for the function
``write_package_version_file()`` there. Otherwise the built-in ``write_basic_package_version_file()``
is used instead, which means losing semantic versioning support.

``COMPATIBILITY``
    Defaults to ``SemanticVersion`` if *Versioning* is available, or ``AnyNewerVersion``
    otherwise.
``NO_VERSION``
    Disables creation of the version info file. All other versioning arguments are ignored.

Parameters for configuring the destination directories:

All the ``_DIR`` arguments work the same as the ``<type> DESTINATION`` parameters of the built-in
``install(TARGETS)`` command. For each empty or missing directory argument as the first fallback the
matching ``CMAKE_INSTALL_<dir>`` global variable (for example set by *GNUInstallDirs*) is tried.
If that is empty as well CMake’s built-in default is used.

Which of the ``_DIR`` arguments is accepted by ``install(TARGETS)`` depends on the CMake version.
For example, ``OBJECTS`` has only been available since v3.9.
#]=]
function(install_package)
    if (NOT DEFINED PROJECT_NAME)
        message(FATAL_ERROR "install_package() must be called after project().")
    endif()

    # ---------- define parameters and parse arguments ----------
    set(switches
        NO_EXPORT NO_VERSION ARCH_INDEPENDENT NO_NAMESPACE
    )
    set(one_value_args
        EXPORT_PREFIX CONFIG_TEMPLATE_FILE
        VERSION COMPATIBILITY
        ARCHIVE_DIR LIBRARY_DIR RUNTIME_DIR OBJECTS_DIR FRAMEWORK_DIR BUNDLE_DIR PUBLIC_HEADER_DIR
          PRIVATE_HEADER_DIR RESOURCE_DIR INCLUDE_DIR
    )
    set(multi_value_args
        TARGETS FIND_DEPENDENCY_CALLS PUBLIC_INCLUDE_DIRS
    )
    cmake_parse_arguments(
        PARSE_ARGV 0 "" "${switches}" "${one_value_args}" "${multi_value_args}"
    )

    # ---------- evaluate arguments, prepare environment for install ----------
    if (_UNPARSED_ARGUMENTS)
        message(FATAL_ERROR "Unknown arguments given: ${_UNPARSED_ARGUMENTS}")
    endif()

    if (NOT _TARGETS)
        message(FATAL_ERROR "At least one target must be given with TARGETS.")
    endif()

    # following dirs are needed for other paths; explicit fallback to GNUInstallDirs variables
    # first, then to CMake’s built-in defaults
    if ("${_LIBRARY_DIR}" STREQUAL "")
        if("${CMAKE_INSTALL_LIBDIR}" STREQUAL "")
            set(_LIBRARY_DIR "lib")
        else()
            set(_LIBRARY_DIR "${CMAKE_INSTALL_LIBDIR}")
        endif()
    endif()
    if ("${_INCLUDE_DIR}" STREQUAL "")
        if("${CMAKE_INSTALL_INCLUDEDIR}" STREQUAL "")
            set(_INCLUDE_DIR "include")
        else()
            set(_INCLUDE_DIR "${CMAKE_INSTALL_INCLUDEDIR}")
        endif()
    endif()

    # MinGW insists on an explicit archive dir
    if (MINGW AND ("${_ARCHIVE_DIR}" STREQUAL ""))
        set(_ARCHIVE_DIR "${_LIBRARY_DIR}")
    endif()

    # export: usual *Targets* files and custom main *Config* file
    if (NOT _NO_EXPORT)
        ppboil_set_if_empty(_EXPORT_PREFIX ${PROJECT_NAME})  # used by Config.cmake.in
        set(export_name "${_EXPORT_PREFIX}Targets")
        set(export_dir "${_LIBRARY_DIR}/cmake/${_EXPORT_PREFIX}")
        set(export_main_filename "${export_name}.cmake")  # used by Config.cmake.in

        ppboil_set_if_empty(
            _CONFIG_TEMPLATE_FILE
            "${ppboil_Installation_cmake_dir}/Installation.Config.cmake.in"
        )
        set(config_file "${_EXPORT_PREFIX}Config.cmake")

        ppboil_make_dependency_calls()

        if (NOT _NO_VERSION)
            set(version_file "${_EXPORT_PREFIX}ConfigVersion.cmake")
            ppboil_set_if_empty(_COMPATIBILITY ${ppboil_Installation_cmake_default_version_compat})
        endif()
    endif()

    # ---------- perform installation ----------

    if (NOT "${_OBJECTS_DIR}" STREQUAL "")
        if (CMAKE_VERSION VERSION_LESS "3.9")
            message(
                FATAL_ERROR
                "install_package(): Argument OBJECTS_DIR requires at least CMake 3.9. "
                "You are running ${CMAKE_VERSION}."
            )
        endif()
        set(objects_dest OBJECTS DESTINATION "${_OBJECTS_DIR}")
    endif()

    install(
        TARGETS ${_TARGETS}
        EXPORT ${export_name}
        ARCHIVE DESTINATION "${_ARCHIVE_DIR}"
        LIBRARY DESTINATION "${_LIBRARY_DIR}"
        RUNTIME DESTINATION "${_RUNTIME_DIR}"
        ${objects_dest}
        FRAMEWORK DESTINATION "${_FRAMEWORK_DIR}"
        BUNDLE DESTINATION "${_BUNDLE_DIR}"
        PUBLIC_HEADER DESTINATION "${_PUBLIC_HEADER_DIR}"
        PRIVATE_HEADER DESTINATION "${_PRIVATE_HEADER_DIR}"
        RESOURCE DESTINATION "${_RESOURCE_DIR}"
        INCLUDES DESTINATION "${_INCLUDE_DIR}"
    )

    if (NOT _NO_EXPORT)
        if (NOT _NO_NAMESPACE)
            set(namespace "${_EXPORT_PREFIX}::")
        endif()

        install(
            EXPORT ${export_name}
            FILE ${export_name}.cmake
            NAMESPACE ${namespace}
            DESTINATION "${export_dir}"
        )
        configure_package_config_file(
            "${_CONFIG_TEMPLATE_FILE}"
            "${config_file}"
            INSTALL_DESTINATION "${export_dir}"
        )
        install(
            FILES "${CMAKE_CURRENT_BINARY_DIR}/${config_file}"
            DESTINATION "${export_dir}"
        )

        if (NOT _NO_VERSION)
            if (_ARCH_INDEPENDENT)
                set(arch ARCH_INDEPENDENT)
            endif()

            if (NOT "${_VERSION}" STREQUAL "")
                ppboil_write_version_file(
                    "${version_file}"
                    VERSION "${_VERSION}"
                    COMPATIBILITY ${_COMPATIBILITY}
                    ${arch}
                )
            else()
                ppboil_write_version_file(
                    "${version_file}"
                    COMPATIBILITY ${_COMPATIBILITY}
                    ${arch}
                )
            endif()

            install(
                FILES "${CMAKE_CURRENT_BINARY_DIR}/${version_file}"
                DESTINATION "${export_dir}"
            )
        endif()
    endif()

    if (_PUBLIC_INCLUDE_DIRS)
        install(
            DIRECTORY ${_PUBLIC_INCLUDE_DIRS}
            DESTINATION "${_INCLUDE_DIR}"
        )
    endif()
endfunction()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# implementation details


# Sets the variable ``var_name`` to ``value`` if it is currently empty or undefined.
# Does nothing otherwise.
macro(ppboil_set_if_empty var_name value)
    if ("${${var_name}}" STREQUAL "")
        set(${var_name} ${value})
    endif()
endmacro()


macro(ppboil_make_dependency_calls)
    # current_find_dependency_calls used by Config.cmake.in
    if (NOT _FIND_DEPENDENCY_CALLS)
        set(current_find_dependency_calls "${${PROJECT_NAME}_FIND_DEPENDENCY_CALLS}")
    else()
        set(current_find_dependency_calls "")
        foreach(dep_calls IN LISTS _FIND_DEPENDENCY_CALLS)
            if(DEFINED "${dep_calls}")
                string(APPEND current_find_dependency_calls "${${dep_calls}}")
            else()
                string(APPEND current_find_dependency_calls "${dep_calls}")
            endif()
        endforeach()
    endif()
    string(
        REPLACE
        "find_dependency(" "find_dependency_w("
        current_find_dependency_calls "${current_find_dependency_calls}"
    )
endmacro()
