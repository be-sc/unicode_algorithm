#[============================================================[
:Author: https://bitbucket.org/be-sc/ppboil
:Version: 2.0.1
:Copyright: CC0/public domain

Common internal helper functions. Not part of ppboil’s public API.
#]============================================================]

# sets: guard_start, guard_end
function(ppboil_make_guard_snippet is_pragma_once include_guard_name)
    if (is_pragma_once)
        set(guard_start "#pragma once" PARENT_SCOPE)
        set(guard_end "" PARENT_SCOPE)
    else()
        set(guard_start
            "#ifndef ${include_guard_name}\n#define ${include_guard_name}"
            PARENT_SCOPE
        )
        set(guard_end
            "\n#endif  // ${include_guard_name}"
            PARENT_SCOPE
        )
    endif()
endfunction()


# sets: namespaces_start, namespaces_end
function(ppboil_make_namespaces_snippet namespaces)
    foreach(ns IN LISTS namespaces)
        string(APPEND start "namespace ${ns}\n{\n")
        string(APPEND end "}  // namespace ${ns}\n")
    endforeach()

    set(namespaces_start "${start}" PARENT_SCOPE)
    set(namespaces_end "${end}" PARENT_SCOPE)
endfunction()
