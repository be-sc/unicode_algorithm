#[============================================================[
:Author: https://bitbucket.org/be-sc/ppboil
:Version: 2.0.2
:Copyright: CC0/public domain

Provides utility functions for versioning including semantic versioning support.
#]============================================================]

set(ppboil_Versioning_cmake_dir "${CMAKE_CURRENT_LIST_DIR}")
include(CMakePackageConfigHelpers)
include("${ppboil_Versioning_cmake_dir}/_internals.cmake")

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#[=[.rst:
Sets a semantic version number for the current project. Intended as a replacement for
``project(VERSION)`` which lacks semver support. Accordingly creates very similar output
variables in the caller’s scope.

The function takes a single parameter, a valid semantic version number according to the
`SemVer 2.0.0 specification <https://semver.org/>`_. The version follows this pattern::

    major.minor.patch-pre+meta

The first three components (the “version triple”) must be present. Each component of the triple must
be a non-negative integer without leading zeros. The pre and meta components are optional.

Outputs (version components):

``<project_name>_VERSION_MAJOR``
    Contains the major version number.
``<project_name>_VERSION_MINOR``
    Contains the minor version number.
``<project_name>_VERSION_PATCH``
    Contains the patch version number.
``<project_name>_VERSION_PRE``
    Contains the pre-release info without the leading dash. Empty if not present.
``<project_name>_VERSION_META``
    Contains the version metadata without the leading plus sign. Empty if not present.

Outputs (version strings):

``<project_name>_VERSION``
    Contains the dot-separated major, minor and patch version, e.g. *1.0.0*. Does not contain
    pre-release or metadata info to remain compatible with the corresponding all-integer variables
    created by the ``project()`` command.
``<project_name>_VERSION_FULL``
    Version up to and including the pre-release component, but without the metadata.
``<project_name>_VERSION_FULL_META``
    Complete version number including the pre-release and metadata components.

Some of the three version string variables might contain the same string. For example, if no
pre-release component is present the ``<project_name>_VERSION`` and ``<project_name>_VERSION_FULL``
variables contain the same string.

Semantic versioning has no equivalent of the numeric ``TWEAK`` version component supported by
the ``project()`` command. ``project_semver()`` does not try to mimic that component.

The pre-release and metadata components are not fully validated because they have no meaning for
version compatibility checks. If you want to validate further yourself you may assume that both
components only contain valid characters.
#]=]
function(project_semver semver)
    parse_semantic_version("${semver}")

    if (ver_error)
        message(FATAL_ERROR "${ver_error} [Version: ${semver}]")
    endif()

    set(${PROJECT_NAME}_VERSION_MAJOR     "${ver_major}"  PARENT_SCOPE)
    set(${PROJECT_NAME}_VERSION_MINOR     "${ver_minor}"  PARENT_SCOPE)
    set(${PROJECT_NAME}_VERSION_PATCH     "${ver_patch}"  PARENT_SCOPE)
    set(${PROJECT_NAME}_VERSION_PRE       "${ver_pre}"    PARENT_SCOPE)
    set(${PROJECT_NAME}_VERSION_META      "${ver_meta}"   PARENT_SCOPE)
    set(${PROJECT_NAME}_VERSION           "${ver_triple}" PARENT_SCOPE)
    set(${PROJECT_NAME}_VERSION_FULL      "${ver_triple}${ver_pre_dash}" PARENT_SCOPE)
    set(${PROJECT_NAME}_VERSION_FULL_META "${ver_triple}${ver_pre_dash}${ver_meta_plus}" PARENT_SCOPE)
endfunction()


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#[=[.rst:
Generates a *ConfigVersion.cmake* file for an installed package.

This function is the same as ``write_basic_package_version_file()`` with added semantic versioning
support. Its signature is almost the same::

    write_package_version_file(
        <output file>
        COMPATIBILITY {<built-in specifier>|SemanticVersion}
        [VERSION {<major.minor.patch>|<semantic version>}]
        [ARCH_INDEPENDENT]
    )

If ``COMPATIBILITY`` is not ``SemanticVersion`` all arguments are simply forwarded to
``write_basic_package_version_file()``. As a result it depends on the CMake version what the list
of ``<built-in specifier>`` is and whether ``ARCH_INDEPENDENT`` is supported.

For ``SemanticVersion`` mode the following applies regardless of the CMake version:

``<output_file>``
    Path to the output file. If it is relative it is interpreted as relative to
    ``CMAKE_CURRENT_BINARY_DIR``.
``VERSION``
    A valid semantic version string. The version of the installed package is reported exactly as
    specified with this argument. However, as per Semver rules, only major and minor version are
    relevant for the compatibility check. Defaults to the current project’s version. Project
    versions set with both ``package(VERSION)`` or ``project_semver()`` are supported.
``ARCH_INDEPENDENT``
    Works similar to the built-in variant. It is intended for header-only libraries or similar
    packages without binaries. If set the architecture of the installed package is not checked.
    Otherwise the architectures must match exactly. For example, an installed package built for
    Linux x86_64 can only be used when building for Linux x86_64 as well.
#]=]
function(write_package_version_file out_file)
    if (NOT out_file)
        message(FATAL_ERROR "Path to output file is missing.")
    endif()

    cmake_parse_arguments(PARSE_ARGV 1 "" "ARCH_INDEPENDENT" "VERSION;COMPATIBILITY" "")

    if (_UNPARSED_ARGUMENTS)
        message(FATAL_ERROR "Unknown keyword arguments given: ${_UNPARSED_ARGUMENTS}")
    endif()

    if (NOT _COMPATIBILITY)
        message(FATAL_ERROR "Argument COMPATIBILITY is missing.")
    endif()

    if (NOT "${_COMPATIBILITY}" STREQUAL "SemanticVersion")
        write_basic_package_version_file(${ARGN})
        return()
    endif()

    # User requrested SemanticVersion mode.

    if (DEFINED _VERSION)
        parse_semantic_version("${_VERSION}")
        set(ver_full_meta "${ver_triple}${ver_pre_dash}${ver_meta_plus}")
    else()
        if (${PROJECT_NAME}_VERSION_FULL_META)  # we got a semver
            set(ver_major     "${${PROJECT_NAME}_VERSION_MAJOR}")
            set(ver_minor     "${${PROJECT_NAME}_VERSION_MINOR}")
            set(ver_patch     "${${PROJECT_NAME}_VERSION_PATCH}")
            set(ver_full_meta "${${PROJECT_NAME}_VERSION_FULL_META}")
        else()
            if (NOT (DEFINED ${PROJECT_NAME}_VERSION_MAJOR AND
                     DEFINED ${PROJECT_NAME}_VERSION_MINOR)
            )
                message(
                    FATAL_ERROR
                    "Project major and/or minor version not set. These two components are required "
                    "for the version compatibility check.")
            endif()

            set(ver_major "${${PROJECT_NAME}_VERSION_MAJOR}")
            set(ver_minor "${${PROJECT_NAME}_VERSION_MINOR}")

            if (${PROJECT_NAME}_VERSION_PATCH)
                set(ver_patch "${${PROJECT_NAME}_VERSION_PATCH}")
            else()
                set(ver_patch 0)
            endif()

            set(ver_full_meta "${ver_major}.${ver_minor}.${ver_patch}")

            # Tweak version might be present, but user requested SemanticVersion mode. Tweak has
            # no meaning there and can be ignored.
        endif()
    endif()

    if (ver_pre)
        set(_EXACT_VERSION_CHECK "
        # Check for exact version match: find_package() does not allow requesting a pre-release
        # version. Since this is a pre-release version we cannot reliably check for an exact
        # version match. We choose the conservative approach and never claim an exact match.")
    else()
        set(_EXACT_VERSION_CHECK "
        if (PACKAGE_VERSION_PATCH EQUAL PACKAGE_FIND_VERSION_PATCH)
            set(PACKAGE_VERSION_EXACT TRUE)
        endif()")
    endif()

    if (_ARCH_INDEPENDENT)
        set(_ARCHITECTURE_CHECK "# Package is architecture independent. No arch check needed.")
    else()
        string(CONFIGURE "
set(_ARCH_BUILT_FOR \"${CMAKE_SYSTEM_NAME}-${CMAKE_SYSTEM_PROCESSOR}\")

if (NOT _ARCH_BUILT_FOR STREQUAL \"\${CMAKE_SYSTEM_NAME}-\${CMAKE_SYSTEM_PROCESSOR}\")
    string(APPEND PACKAGE_VERSION \" (\${_ARCH_BUILT_FOR})\")
    set(PACKAGE_VERSION_UNSUITABLE TRUE)
endif()
" _ARCHITECTURE_CHECK @ONLY)
    endif()

    configure_file(
        "${ppboil_Versioning_cmake_dir}/Versioning.ConfigVersion.cmake.in"
        "${out_file}"
        @ONLY
    )
endfunction()


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#[=[.rst:
Creates a header file with version information usable from both C++ and C. Usage::

    write_version_header(
        <output_file>
        [VERSION {<semver>|<major.minor.patch.tweak>}]
        [MACRO_PREFIX <name>]
        [NAMESPACES <ns1> ...]
        [PRAGMA_ONCE | INCLUDE_GUARD_NAME <name>]
        [PREAMBLE <content>]
        [APPENDIX <content>]
    )

Parameters:

``<output_file>``
    Path to the output file. If it is relative it is interpreted relative to
    ``CMAKE_CURRENT_BINARY_DIR``.
``VERSION``
    Either a semantic version string or a numeric version with 1–4 components. Defaults to the
    current project’s version (can set by either ``project(VERSION)`` or ``project_semver()``).
``MACRO_PREFIX``
    Prefix for macro names. Automatically converted to upper case. Defaults to to ``PROJECT_NAME``.
``NAMESPACES``
    The C++ namespace hierarchy version constants are put into. If empty the constants are put into
    the global namespace.
``PRAGMA_ONCE`` and ``INCLUDE_GUARD_NAME <guard>``
    Configure the include guard style used by the header file. Defaults to an include guard macro
    with a name derived from ``MACRO_PREFIX``.
``PREAMBLE <content>``
    Inserted verbatim at the very beginning of the generated header, i.e. *before* the include
    guard. Intended for adding a start-of-file comment.
``APPENDIX <content>``
    Inserted verbatim at the end of the generated header *before* closing the include guard. Any
    custom code you want to have in the header should go here.
#]=]
function(write_version_header output_file)
    if (NOT output_file)
        message(FATAL_ERROR "Path to output file is missing.")
    endif()

    set(switches PRAGMA_ONCE)
    set(one_value_args VERSION MACRO_PREFIX INCLUDE_GUARD_NAME PREAMBLE APPENDIX)
    set(multi_value_args NAMESPACES)
    cmake_parse_arguments(PARSE_ARGV 1 "" "${switches}" "${one_value_args}" "${multi_value_args}")

    if (_UNPARSED_ARGUMENTS)
        message(FATAL_ERROR "Unknown keyword arguments given: ${_UNPARSED_ARGUMENTS}")
    endif()

    if (NOT _MACRO_PREFIX)
        set(_MACRO_PREFIX "${PROJECT_NAME}")
    endif()
    string(MAKE_C_IDENTIFIER "${_MACRO_PREFIX}" _MACRO_PREFIX)
    string(TOUPPER "${_MACRO_PREFIX}" _MACRO_PREFIX)

    if (NOT _INCLUDE_GUARD_NAME)
        set(_INCLUDE_GUARD_NAME "${_MACRO_PREFIX}_VERSION_INFO")
    endif()

    ppboil_version_header_prepare_version()
    ppboil_make_guard_snippet(${_PRAGMA_ONCE} "${_INCLUDE_GUARD_NAME}")
    ppboil_make_namespaces_snippet("${_NAMESPACES}")

    if (_PREAMBLE)
        string(APPEND _PREAMBLE "\n")
    endif()

    if (_APPENDIX)
        string(APPEND _APPENDIX "\n")
    endif()

    if (namespaces_start)
        set(indent "    ")
    endif()

    ppboil_append_ver_num("_MAJOR" "_major" ver_major)
    ppboil_append_ver_num("_MINOR" "_minor" ver_minor)
    ppboil_append_ver_num("_PATCH" "_patch" ver_patch)

    if (ver_is_semantic)
        ppboil_append_ver_str("_PRE" "_pre" ver_pre)
        ppboil_append_ver_str("_META" "_meta" ver_meta)
    else()
        ppboil_append_ver_num("_TWEAK" "_tweak" ver_tweak)
    endif()

    ppboil_append_ver_str("" "" ver_full)

    configure_file(
        "${ppboil_Versioning_cmake_dir}/Versioning.version.header.in"
        "${output_file}"
        @ONLY
    )
endfunction()



# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#[=[.rst:
Parses a semantic version string.

The function takes a single parameter, a valid semantic version number according to the
`SemVer 2.0.0 specification <https://semver.org/>`_. The version follows this pattern::

    major.minor.patch-pre+meta

The first three components (the “version triple”) must be present. Each component of the triple
must be a non-negative integer without leading zeros. The pre and meta components are optional.

Outputs:

``ver_error``
    Empty if parsing succeeded. Otherwise contains an error message. In the error case the state of
    the other output variables is undefined. Do not rely on them.
``ver_major``, ``ver_minor``, ``ver_patch``
    The major, minor and patch version numbers.
``ver_triple``
    The major, minor and patch version combined into an *x.y.z* version triple string.
``ver_pre``, ``ver_pre_dash``
    The pre-release component excluding and including the leading dash. Both variables are empty
    if the version has no pre-release component.
``ver_meta``, ``ver_meta_plus``
    The metadata component excluding and including the leading plus sign. Both variables are empty
    if the version has no metadata component.
#]=]
function(parse_semantic_version semver_string)
    string(STRIP "${semver_string}" semver_string)

    if (semver_string STREQUAL "")
        set(ver_error "Version cannot be emtpy." PARENT_SCOPE)
        return()
    endif()

    if (semver_string MATCHES "^([0-9.]+)(-[0-9A-Za-z.\-]+)?([+][0-9A-Za-z.\-]+)?$")
        set(ver_triple "${CMAKE_MATCH_1}")

        if (CMAKE_MATCH_2)
            string(SUBSTRING "${CMAKE_MATCH_2}" 1 -1 ver_pre)
            set(ver_pre "${ver_pre}" PARENT_SCOPE)
            set(ver_pre_dash "${CMAKE_MATCH_2}" PARENT_SCOPE)
        endif()

        if (CMAKE_MATCH_3)
            string(SUBSTRING "${CMAKE_MATCH_3}" 1 -1 ver_meta)
            set(ver_meta "${ver_meta}" PARENT_SCOPE)
            set(ver_meta_plus "${CMAKE_MATCH_3}" PARENT_SCOPE)
        endif()
    endif()

    ppboil_parse_dotted_version("${ver_triple}")

    if (ver_len LESS 3)
        set(ver_error "Version must at least have major.minor.patch components." PARENT_SCOPE)
        return()
    elseif (ver_len GREATER 3)
        set(ver_error
            "Version cannot have more than the three dotted components major.minor.patch."            PARENT_SCOPE
        )
        return()
    elseif (ver_error)
        set(ver_error "${ver_error}" PARENT_SCOPE)
        return()
    endif()

    set(ver_triple "${ver_triple}" PARENT_SCOPE)
    set(ver_major  "${ver_major}"  PARENT_SCOPE)
    set(ver_minor  "${ver_minor}"  PARENT_SCOPE)
    set(ver_patch  "${ver_patch}"  PARENT_SCOPE)
endfunction()


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#[=[.rst:
Parses a numeric version number with up to four components: *major.minor.patch.tweak*.

Each component must be a non-negative integer without leading zeros. The major version must be
present, the other three are optional.

Outputs:

``ver_error``
    Empty if parsing succeeded. Otherwise contains an error message. In the error case the state of
    the other output variables is undefined. Do not rely on them.
``ver_major``, ``ver_minor``, ``ver_patch``, ``ver_tweak``
    The four components of the version.
``ver_len``
    The number of components contained in the version (1–4).
``ver_triple``
    The major, minor and patch version combined into an *x.y.z* version triple string.
#]=]
function(parse_numeric_version version)
    string(STRIP "${version}" version)

    if (version STREQUAL "")
        set(ver_error "Version cannot be emtpy." PARENT_SCOPE)
        return()
    endif()

    ppboil_parse_dotted_version("${version}")

    if (ver_error)
        set(ver_error "${ver_error}" PARENT_SCOPE)
        return()
    elseif ((ver_len LESS 1) OR (ver_len GREATER 4))
        set(ver_error "Version must have 1–4 components, but had ${ver_len}." PARENT_SCOPE)
        return()
    endif()

    set(ver_len ${ver_len} PARENT_SCOPE)
    set(ver_major "${ver_major}" PARENT_SCOPE)
    set(ver_minor "${ver_minor}" PARENT_SCOPE)
    set(ver_patch "${ver_patch}" PARENT_SCOPE)
    set(ver_tweak "${ver_tweak}" PARENT_SCOPE)
endfunction()


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#[=[.rst:
Parses any kind of supported version string.

First tries to parse the string as a semantic version. If that fails tries parsing as a numeric
version. Because of this parsing order versions of the form *major.minor.patch* are always
recognized as semantic versions.

Outputs:

``ver_error``
    Empty if parsing succeeded. Otherwise contains the combined error messages from both parsing
    attempts. In the error case the state of the other output variables is undefined. Do not rely
    on them.
``ver_is_semantic``
    True if the version was successfully parsed as a semantic version, false otherwise.
*parsing result*
    Also defines output variables depending on which parsing attempt succeeded. See
    ``parse_semantic_version()`` and ``parse_numeric_version()`` for details.
#]=]
macro(parse_any_version version)
    parse_semantic_version("${version}")

    if (ver_error)
        set(_ver_error_sem "${ver_error}")
        unset(ver_error)
        set(ver_is_semantic FALSE)

        parse_numeric_version("${version}")

        if (ver_error)
            set(ver_error
                "Unrecognized version format. Not a semantic version because: '${_ver_error_sem}'. "
                "Not a numeric version because: '${ver_error}'."
            )
        endif()

        unset(_ver_error_sem)
    else()
        set(ver_is_semantic TRUE)
    endif()
endmacro()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# implementation details


# version is a set of numbers separated by dots
# sets: ver_error, ver_len, ver_major, ver_minor, ver_patch, ver_tweak
function(ppboil_parse_dotted_version version)
    if (NOT version MATCHES "^(0|([1-9][0-9]*))(\.(0|([1-9][0-9]*)))*$")
        set(ver_error
            "Numeric version components must be non-negative integers without leading zeros."
            PARENT_SCOPE)
        return()
    endif()

    string(REPLACE "." ";" component_list "${version}")
    list(LENGTH component_list ver_len)
    set(ver_len ${ver_len} PARENT_SCOPE)

    if (ver_len GREATER 0)
        list(GET component_list 0 ver_major)
        set(ver_major "${ver_major}" PARENT_SCOPE)
    endif()

    if (ver_len GREATER 1)
        list(GET component_list 1 ver_minor)
        set(ver_minor "${ver_minor}" PARENT_SCOPE)
    endif()

    if (ver_len GREATER 2)
        list(GET component_list 2 ver_patch)
        set(ver_patch "${ver_patch}" PARENT_SCOPE)
    endif()

    if (ver_len GREATER 3)
        list(GET component_list 3 ver_tweak)
        set(ver_tweak "${ver_tweak}" PARENT_SCOPE)
    endif()
endfunction()


# requires: _VERSION
# sets: all ver_*
macro(ppboil_version_header_prepare_version)
    if (NOT "${_VERSION}" STREQUAL "")
        parse_any_version("${_VERSION}")

        if (ver_error)
            message(FATAL_ERROR "${ver_error} [version: ${_VERSION}]")
        endif()

        if (ver_is_semantic)
            set(ver_full "${ver_triple}${ver_pre_dash}")
        else()
            set(ver_full "${_VERSION}")
        endif()
    else()
        if ("${${PROJECT_NAME}_VERSION_MAJOR}" STREQUAL "")
            message(FATAL_ERROR "No project version defined. [project: ${PROJECT_NAME}]")
        endif()

        set(ver_major "${${PROJECT_NAME}_VERSION_MAJOR}")
        set(ver_minor "${${PROJECT_NAME}_VERSION_MINOR}")
        set(ver_patch "${${PROJECT_NAME}_VERSION_PATCH}")
        set(ver_tweak "${${PROJECT_NAME}_VERSION_TWEAK}")
        set(ver_pre   "${${PROJECT_NAME}_VERSION_PRE}")
        set(ver_meta  "${${PROJECT_NAME}_VERSION_META}")

        if (${PROJECT_NAME}_VERSION_FULL_META)  # we have a semver
            set(ver_is_semantic TRUE)
            set(ver_full "${${PROJECT_NAME}_VERSION_FULL}")
        else()
            set(ver_is_semantic FALSE)
            set(ver_full "${${PROJECT_NAME}_VERSION}")
        endif()
    endif()
endmacro()


macro(ppboil_append_ver_num COMP comp value_var)
    if (${value_var} GREATER_EQUAL 0)
        string(
            APPEND ver_defines
            "#define ${_MACRO_PREFIX}_VERSION${COMP} ${${value_var}}\n"
        )
        string(
            APPEND ver_constants
            "${indent}constexpr const unsigned version${comp} = ${${value_var}};\n"
        )
    endif()
endmacro()


macro(ppboil_append_ver_str COMP comp value_var)
    if (NOT "${${value_var}}" STREQUAL "")
        string(
            APPEND ver_defines
            "#define ${_MACRO_PREFIX}_VERSION${COMP} \"${${value_var}}\"\n"
        )
        string(
            APPEND ver_constants
            "${indent}constexpr const char version${comp}[] = \"${${value_var}}\";\n"
        )
    endif()
endmacro()
