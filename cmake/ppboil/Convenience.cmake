#[============================================================[
:Author: https://bitbucket.org/be-sc/ppboil
:Version: 2.0.1
:Copyright: CC0/public domain

Assorted small helper function to reduce boilerplate and make life more pleasant. :)
#]============================================================]

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#[=[
Same as the built-in ``option()``, but also prints the option’s value as a status message.
#]=]
macro(reported_option optname title description default)
    option(${optname} "${description}" ${default})
    message(STATUS "Building ${title}: ${${optname}}")
endmacro()


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#[=[
Adds a library and a namespaced alias for it.

Takes the same arguments as the built-in ``add_library()``. ``double_coloned_name`` is the fully
qualified name of the library, using a double colon as the delimiter: e.g. ``namespace::library``.
Each double colons is transformed into a single underscore and the resulting name is used as the
actual name of the added library. The double coloned name is used to create the namespaced alias.

Output variable:

``underscored_name``
    Contains the library name with double colons transformed into underscores.
#]=]
macro(add_namespaced_library double_coloned_name)
    string(REPLACE "::" "_" underscored_name ${double_coloned_name})
    add_library(${underscored_name} ${ARGN})
    add_library(${double_coloned_name} ALIAS ${underscored_name})
endmacro()
