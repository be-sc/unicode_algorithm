/* Part of https://gitlab.com/be-sc/unicode_algorithm. Licence: MIT. See LICENSE.rst. */
#include <array>
#include <cstdint>
#include <unicode_algorithm/unicode.hpp>

#include "unicode.cpp.in"  // generated file

namespace unicode
{

using std::uint8_t;

namespace
{
// number of enumerators in ``boundary_class`` excluding ``regional_indicator``.
constexpr const uint8_t connects_count = 13;

// rules according to UAX #29; lines are lhs, columns are rhs
// clang-format off
constexpr const std::array<std::array<bool, connects_count>, connects_count> connects{{
    /*        othr    cr     lf     ctrl   pre    ext    spc    zwj    l      v      t      lv     lvt */
    {/*othr*/ false,  false, false, false, false, true,  true,  true,  false, false, false, false, false},
    {/*cr  */ false,  false, true,  false, false, false, false, false, false, false, false, false, false},
    {/*lf  */ false,  false, false, false, false, false, false, false, false, false, false, false, false},
    {/*ctrl*/ false,  false, false, false, false, false, false, false, false, false, false, false, false},
    {/*pre */ true,   false, false, false, true,  true,  true,  true,  true,  true,  true,  true,  true},
    {/*ext */ false,  false, false, false, false, true,  true,  true,  false, false, false, false, false},
    {/*spc */ false,  false, false, false, false, true,  true,  true,  false, false, false, false, false},
    {/*zwj */ false,  false, false, false, false, true,  true,  true,  false, false, false, false, false},
    {/*l   */ false,  false, false, false, false, true,  true,  true,  true,  true,  false, true,  true,},
    {/*v   */ false,  false, false, false, false, true,  true,  true,  false, true,  true,  false, false},
    {/*t   */ false,  false, false, false, false, true,  true,  true,  false, false, true,  false, false},
    {/*lv  */ false,  false, false, false, false, true,  true,  true,  false, true,  true,  false, false},
    {/*lvt */ false,  false, false, false, false, true,  true,  true,  false, false, true,  false, false},
}};
// clang-format on
}  // namespace

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

const character_properties& code_point::properties() const
{
    using namespace char_props;
    return stage3[stage2[stage1[m_value >> 8] + m_value & 0xff]];
}


bool grapheme_continues(code_point lhs, code_point rhs)
{
    const auto class_lhs = lhs.properties().boundary_class;
    const auto class_rhs = rhs.properties().boundary_class;

    if (class_lhs == boundary_class::regional_indicator
        || class_rhs == boundary_class::regional_indicator) {
        // Regional_indicator and extended_pictographic omitted.
        // Rules GB11-13 need special handling because of their complexity.
        return false;  // bescbau
    }

    return connects[static_cast<uint8_t>(class_lhs)][static_cast<uint8_t>(class_rhs)];
}

}  // namespace unicode
