#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

if sys.version_info < (3, 6):
    raise RuntimeError(
        'At least Python 3.6 required. You are running {}.{}.{}'.format(*sys.version_info))

import itertools
import re
from pathlib import Path, PurePath
from typing import Dict, Optional, List, Sequence, Tuple
from urllib import request

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

UNICODE_VERSION = '12.1.0'
UCD_URL = f'https://www.unicode.org/Public/{UNICODE_VERSION}/ucd/'
UCD_FILEPATHS = [  # relative to UCD_URL
    'auxiliary/GraphemeBreakProperty.txt'
]


BOUNDARY_CLASS_OTHER = 'Other'

# keys are exactly as they appear in GraphemeBreakProperty.txt
# values are the enumerators from the unicode::boundary_class enum
BOUNDARY_CLASS_ENUM_LUT: Dict[str, str] = {
    BOUNDARY_CLASS_OTHER: 'other',
    'CR'                : 'carriage_return',
    'LF'                : 'line_feed',
    'Control'           : 'control',
    'Prepend'           : 'prepend',
    'Extend'            : 'extend',
    'SpacingMark'       : 'spacing_mark',
    'ZWJ'               : 'zero_width_joiner',
    'Regional_Indicator': 'regional_indicator',
    'L'                 : 'hangul_l',
    'V'                 : 'hangul_v',
    'T'                 : 'hangul_t',
    'LV'                : 'hangul_lv',
    'LVT'               : 'hangul_lvt',
}
BOUNDARY_CLASS_NAMES: List[str] = list(BOUNDARY_CLASS_ENUM_LUT.keys())


SCRIPT_DIRPATH: Path = Path(__file__).resolve().parent
CPP_FILEPATH: Path = SCRIPT_DIRPATH / '..' / 'src' / 'unicode.cpp.in'

CPP_TMPL_TOP = """/* Part of https://gitlab.com/be-sc/unicode_algorithm. Licence: MIT. See LICENSE.rst. */
/*
GENERATED FILE! Manual edits will be lost!
Generation script: {gen_filename}
*/
// clang-format off

namespace unicode
{{
namespace char_props
{{

constexpr const std::array<std::uin16_t, {stage1_size}> stage1{{
"""
CPP_TMPL_MIDDLE1 = """}};

constexpr const std::array<std::uin16_t, {stage2_size}> stage2{{
"""
CPP_TMPL_MIDDLE2 = """}};

constexpr const std::array<character_properties, {stage3_size}> stage3{{
"""
CPP_TMPL_BOTTOM = """};

}  // namespace char_props
}  // namespace unicode
"""
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

class Tables:
    BLOCK_SIZE = 256

    def __init__(self):
        self.blocks_lut: Dict[Tuple, int] = {}
        self.stage1: List[int] = []
        self.stage2: List[int] = []
        self.stage3: List[str] = []


def main(unidata_dir: Path, update: bool):
    if update:
        print('Downloading Unicode character data files ...')
        update_unicode_data_files(unidata_dir)

    print('Processing character data files ...')
    boundary_classes = parse_boundary_classes(unidata_dir)
    tables = build_tables(unidata_dir, boundary_classes)

    print('Generating C++ code ...')
    generate_cpp(tables)


def intx(s: str) -> int:
    """ Converts a hexadecimal string into an integer. """
    return int(s, base=16)


def parse_boundary_classes(unidata_dir: Path) -> Dict[int, str]:
    """
    Parses the boundary classes needed to determine the boundaries of graphemes, words, etc.
    The returned dictionary means: key: code point, value: boundary class name. Every code point
    not in that dictionary has boundary class *Other*.
    """
    filename = 'GraphemeBreakProperty.txt'
    result = {}
    cp_count = 0

    def add_boundary_class(cpoint: int, bclass: str):
        if cpoint in result:
            raise ValueError(
                f'{filename}, line {line_num}: Code point {hex(cpoint)} parsed previously.')
        if bclass not in BOUNDARY_CLASS_NAMES:
            raise ValueError(f'{filename}, line {line_num}: Unknown boundary class: {bclass}')
        result[cpoint] = bclass

    # Matches lines used for error checking, like:
    # Total code points: 137
    cp_count_re = re.compile(r'^# Total code points: (?P<count>[0-9]+)')

    # Matches line with a single code point, like:
    # 00AD          ; Control # Cf       SOFT HYPHEN
    single_cp_re = re.compile(r'^(?P<cp>[0-9A-F]{4,5})\s*;\s*(?P<cls>[^\s]+)')

    # Matches line with a code point range, like:
    # AA29..AA2E    ; Extend # Mn   [6] CHAM VOWEL SIGN AA..CHAM VOWEL SIGN OE
    range_cp_re = re.compile(r'^(?P<cp1>[0-9A-F]{4,5})\.\.(?P<cp2>[0-9A-F]{4,5})\s*;\s*(?P<cls>[^\s]+)')

    with (unidata_dir / filename).open(mode='r', encoding='utf-8') as fp:
        for line_num, line in enumerate(fp, start=1):
            match = cp_count_re.match(line)
            if match is not None:
                cp_count += int(match['count'])
                continue

            if line.startswith('#') or line.isspace():
                continue

            match = single_cp_re.match(line)
            if match is not None:
                add_boundary_class(intx(match['cp']), match['cls'])
            else:
                match = range_cp_re.match(line)
                if match is None:
                    raise ValueError(f'{filename}, line {line_num}: Line has unknown format.')
                for cp in range(intx(match['cp1']), intx(match['cp2']) + 1):
                    add_boundary_class(cp, match['cls'])

    if len(result) != cp_count:
        raise ValueError(
            f'{filename}: Expected {cp_count} code points, parsed {len(result)} code points.')

    return result


def build_tables(unidata_dir: Path, boundary_classes: Dict[int, str]) -> Tables:
    """
    Builds the compressed multi-stage trie data structure (the tables) used for characater lookup.
    """
    tables = Tables()
    block = []

    # For now only boundary class data is needed. That’s why unidata_dir is unused.
    # For the future expect this to be a loop over UnicodeData.txt
    for cpoint in range(0, 0x110000):
        enumerator = BOUNDARY_CLASS_ENUM_LUT[boundary_classes.get(cpoint, BOUNDARY_CLASS_OTHER)]

        try:
            enumerator_idx = tables.stage3.index(enumerator)
        except ValueError:
            enumerator_idx = len(tables.stage3)
            tables.stage3.append(enumerator)

        block.append(enumerator_idx)

        if len(block) == Tables.BLOCK_SIZE or cpoint == 0x10FFFF:
            block = tuple(block)  # used as a dict key, i.e. must be hashable
            start_idx = tables.blocks_lut.get(block)

            if start_idx is None:
                start_idx = len(tables.stage2)
                tables.stage2.extend(block)
                tables.blocks_lut[block] = start_idx

            tables.stage1.append(start_idx)
            block = []

    errors = ''
    if len(tables.stage1) > 2**16:
        errors += 'Stage1 has too many items for uint16_t.'
    if len(tables.stage2) > 2**16:
        errors += 'Stage2 has too many items for uint16_t.'

    if errors:
        raise RuntimeError(errors)

    return tables


class GroupsOf:
    def __init__(self, group_size: int):
        assert group_size > 0
        self.group_size: int = group_size
        self.counter: int = 0
        self.group_id: int = 0

    def __call__(self, _):
        self.counter += 1

        if self.counter > self.group_size:
            self.group_id += 1
            self.counter = 1

        return self.group_id


def generate_cpp(tables: Tables) -> None:
    with CPP_FILEPATH.open(mode='w', encoding='utf-8', newline='\n') as cpp:
        cpp.write(CPP_TMPL_TOP
                  .format(gen_filename=PurePath(__file__).name, stage1_size=len(tables.stage1)))

        for _, s1group in itertools.groupby(tables.stage1, GroupsOf(20)):
            s1group = [str(x) for x in s1group]
            cpp.write(f'    {", ".join(s1group)}\n')

        cpp.write(CPP_TMPL_MIDDLE1.format(stage2_size=len(tables.stage2)))

        for _, s2group in itertools.groupby(tables.stage2, GroupsOf(20)):
            s2group = [str(x) for x in s2group]
            cpp.write(f'    {", ".join(s2group)}\n')

        cpp.write(CPP_TMPL_MIDDLE2.format(stage3_size=len(tables.stage3)))

        for enumerator in tables.stage3:
            cpp.write(f'    {{boundary_class::{enumerator}}},\n')

        cpp.write(CPP_TMPL_BOTTOM)


def update_unicode_data_files(unidata_dir: Path) -> None:
    """
    Downloads the required files of the character database and stores them in ``unidata_dir``.
    """
    unidata_dir.mkdir(parents=True, exist_ok=True)

    for filepath in UCD_FILEPATHS:
        dnld_url = UCD_URL + filepath
        dest_filepath = unidata_dir / Path(filepath).name

        with open(dest_filepath, mode='bw') as dest:
            with request.urlopen(dnld_url, timeout=15) as dnld:
                dest.write(dnld.read())



def parse_cli() -> Tuple[Path, bool]:
    import argparse
    parser = argparse.ArgumentParser()

    unidata_dirpath = str(SCRIPT_DIRPATH / 'ucd')
    parser.add_argument(
        '-d', '--data-dir', dest='data_dir', default=unidata_dirpath,
        help=f'Directory where the unicode data files are located. Defaults to: {unidata_dirpath}'
    )

    parser.add_argument(
        '-u', '--update', action='store_true',
        help='Updates the character database from the Unicode web site before generating.'
    )

    cfg = parser.parse_args()
    return (Path(cfg.data_dir), cfg.update)


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


if __name__ == '__main__':
    try:
        main(*parse_cli())
    except KeyboardInterrupt:
        sys.exit(1)
