/* Part of https://gitlab.com/be-sc/unicode_algorithm. Licence: MIT. See LICENSE.rst. */
/**
This file contains internal implementation details only. Nothing here is part of the public API.
*/
#pragma once

#include "../unicode.hpp"
#include "../utf8_defs.hpp"
#include <cassert>
#include <iterator>

namespace unicode
{
namespace utf8
{
namespace internal
{

using byte_type = std::uint8_t;
constexpr static const byte_type start_of_bom{0xef};


inline bool is_tail_byte(byte_type byte)
{
    return (byte & 0b11000000) == 0b10000000;
}


// Can’t rule out that ByteIter points to something signed. Then the cast may be necessary
// to avoid compiler warnings.
template<typename ByteIter>
inline byte_type deref8(ByteIter pos)
{
    return static_cast<byte_type>(*pos);
}


code_unit_role analyze_code_unit(byte_type cunit)
{
    // see RFC 3629 section 1 and 3

    if (cunit < 0x80) {
        return code_unit_role::ascii;
    }

    if (cunit < 0xc0) {
        return code_unit_role::tail;
    }

    if (cunit > 0xc1 && cunit < 0xe0) {  // C0, C1 never appear
        return code_unit_role::head2;
    }

    if (cunit < 0xf0) {
        return code_unit_role::head3;
    }

    if (cunit < 0xf5) {
        return code_unit_role::head4;
    }

    return code_unit_role::invalid;
}


/*
Tries to recover from a data error by searching for the next byte that is valid UTF-8 data and
not a tail byte. That’s the start of the next valid character after the error. Iterates till the
end if such a position does not exist.
*/
template<typename ByteIter>
void recover(ByteIter& pos, const ByteIter& end)
{
    while (pos != end) {
        ++pos;

        switch (analyze_code_unit(deref8(pos))) {
            case code_unit_role::tail:
                break;
            case code_unit_role::invalid:
                break;
            default:
                return;  // found viable position
        }
    }
}


inline code_point encode_error(error_reason e)
{
    return code_point(static_cast<code_point::value_type>(e));
}


inline bool is_error_code(code_point cp)
{
    return cp.get() >= static_cast<code_point::value_type>(error_reason::invalid_data);
}


template<unsigned len, typename ByteIter>
code_point decode_multibyte_sequence(ByteIter& pos, const ByteIter& end)
{
    static_assert(len >= 2 && len <= 4);
    assert(std::distance(pos, end) >= 0);
    using val_t = code_point::value_type;

    if (std::distance(pos, end) < len) {
        return encode_error(error_reason::incomplete_code_point);
    }

    constexpr const byte_type mask = 0xff >> (len + 1);
    val_t cp = static_cast<val_t>(deref8(pos) & mask) << (6 * (len - 1));
    ++pos;

    for (unsigned i = 2; i <= len; ++i) {
        const byte_type cunit = deref8(pos);

        if (not is_tail_byte(cunit)) {
            return encode_error(error_reason::incomplete_code_point);
        }

        cp &= static_cast<val_t>(cunit & 0b00111111) << (6 * (len - i));
        ++pos;
    }

    return code_point(cp);
}


// decodes a single code point
// postcondition: pos points to the code unit immediately after the decoded sequence
template<typename ByteIter>
code_point decode_code_point(ByteIter& pos, const ByteIter& end)
{
    assert(pos != end);

    switch (analyze_code_unit(deref8(pos))) {
        case code_unit_role::ascii:
            ++pos;
            return code_point(static_cast<code_point::value_type>(*pos));

        case code_unit_role::head2:
            return decode_multibyte_sequence<2>(pos, end);

        case code_unit_role::head3:
            return decode_multibyte_sequence<3>(pos, end);

        case code_unit_role::head4:
            return decode_multibyte_sequence<4>(pos, end);

        case code_unit_role::invalid:
            return encode_error(error_reason::invalid_data);

        case code_unit_role::tail:
            return encode_error(error_reason::unexpected_tail);
    }
}


// Skips BOM and broken data.
// Advances `pos` to the first valid non-BOM position or to `end`. Sequence may be empty.
template<typename ByteIter>
void find_start_of_text(ByteIter& pos, ByteIter end)
{
    if (pos == end) {
        return;
    }

    switch (internal::analyze_code_unit(internal::deref8(pos))) {
        case code_unit_role::ascii:
            [[fallthrough]];
        case code_unit_role::head2:
            [[fallthrough]];
        case code_unit_role::head4:
            break;  // valid non-BOM start of text

        case code_unit_role::tail:
            [[fallthrough]];
        case code_unit_role::invalid:
            // invalid start of text
            internal::recover(pos, end);
            break;

        case code_unit_role::head3: {
            // text might start with BOM
            if (deref8(pos) != 0xef) {
                break;  // not a BOM
            }

            if (std::distance(pos, end) < 3) {
                // text consists of an incomplete BOM only
                pos = end;
                break;
            }

            ++pos;

            if (deref8(pos) != 0xbb) {
                recover(pos, end);
                break;
            }

            ++pos;

            if (deref8(pos) != 0xbf) {
                recover(pos, end);
                break;
            }

            ++pos;
            break;
        }
    }
}


}  // namespace internal
}  // namespace utf8
}  // namespace unicode
