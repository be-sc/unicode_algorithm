/* Part of https://gitlab.com/be-sc/unicode_algorithm. Licence: MIT. See LICENSE.rst. */
/**
This file contains internal implementation details only. Nothing here is part of the public API.
*/
#pragma once

namespace unicode
{
namespace internal
{

template<typename T, typename Tag>
class quantity
{
public:
    using value_type = T;

    constexpr quantity() noexcept = default;
    constexpr explicit quantity(T value) noexcept : m_value(value) {}

    constexpr T get() const noexcept { return m_value; }

    constexpr bool operator==(quantity rhs) const noexcept { return this->m_value == rhs.m_value; }
    constexpr bool operator!=(quantity rhs) const noexcept { return this->m_value != rhs.m_value; }
    constexpr bool operator<(quantity rhs) const noexcept { return this->m_value < rhs.m_value; }
    constexpr bool operator>(quantity rhs) const noexcept { return this->m_value > rhs.m_value; }
    constexpr bool operator<=(quantity rhs) const noexcept { return this->m_value <= rhs.m_value; }
    constexpr bool operator>=(quantity rhs) const noexcept { return this->m_value >= rhs.m_value; }

    constexpr quantity& operator++() noexcept
    {
        ++m_value;
        return *this;
    }
    constexpr quantity operator++(int) noexcept
    {
        quantity tmp(*this);
        ++(*this);
        return tmp;
    }

    constexpr quantity& operator--() noexcept
    {
        --m_value;
        return *this;
    }
    constexpr quantity operator--(int) noexcept
    {
        quantity tmp(*this);
        --(*this);
        return tmp;
    }

    constexpr quantity& operator+=(quantity rhs)
    {
        m_value += rhs.m_value;
        return *this;
    }

    constexpr quantity operator+(quantity rhs)
    {
        quantity tmp(*this);
        tmp += rhs;
        return tmp;
    }

    constexpr quantity& operator-=(quantity rhs)
    {
        m_value -= rhs.m_value;
        return *this;
    }

    constexpr quantity operator-(quantity rhs)
    {
        quantity tmp(*this);
        tmp -= rhs;
        return tmp;
    }

private:
    T m_value{};
};


}  // namespace internal
}  // namespace unicode
