/* Part of https://gitlab.com/be-sc/unicode_algorithm. Licence: MIT. See LICENSE.rst. */
#pragma once

#include "internal/utf8_internal.hpp"
#include "unicode.hpp"
#include "utf8_defs.hpp"
#include <cassert>
#include <cstddef>
#include <iterator>
#include <string_view>
#include <utility>

namespace unicode
{
namespace utf8
{

/**
Iterator over user-perceived characters.
Takes into account multi code unit characters and grapheme clusters.
*/
template<typename ByteIter>
class character_iterator
{
    using ByteIterTraits = typename std::iterator_traits<ByteIter>;

public:
    using byte_iterator = ByteIter;
    using value_type = typename ByteIterTraits::value_type;
    using pointer = typename ByteIterTraits::pointer;
    using reference = typename ByteIterTraits::reference;
    using difference_type = typename ByteIterTraits::difference_type;
    using iterator_category = std::bidirectional_iterator_tag;

    static_assert(
            sizeof(value_type) == size_of_code_unit,
            "ByteIter must be an iterator over UTF-8 code units (bytes).");
    static_assert(
            std::is_base_of_v<
                    std::bidirectional_iterator_tag,
                    typename ByteIterTraits::iterator_category>,
            "ByteIter must be at least bidirectional.");

    constexpr static bool is_const() { return std::is_const_v<value_type>; }


    explicit character_iterator(ByteIter first, ByteIter past_last)
        : m_pos(std::forward<ByteIter>(first)), m_end(std::forward<ByteIter>(past_last))
    {
        internal::find_start_of_text(m_pos, m_end);
    }


    bool is_at_end() const { return m_pos == m_end; }


    /** Access to the current code unit (byte) */
    /// @{
    const value_type& operator*() const
    {
        assert(not is_at_end());
        return *m_pos;
    }

    std::enable_if_t<not is_const(), value_type&> operator*()
    {
        assert(not is_at_end());
        return *m_pos;
    }

    const value_type* operator->() const
    {
        assert(not is_at_end());

        if constexpr (std::is_pointer_v<ByteIter>) {
            return m_pos;
        }
        else {
            return m_pos.operator->();
        }
    }

    std::enable_if_t<not is_const(), value_type*> operator->()
    {
        assert(not is_at_end());

        if constexpr (std::is_pointer_v<ByteIter>) {
            return m_pos;
        }
        else {
            return m_pos.operator->();
        }
    }
    /// @}


    /** Advances to the next character (1..n bytes). */
    /// @{
    character_iterator& operator++()
    {
        assert(not is_at_end());

        while (true) {
            const code_point cp = internal::decode_code_point(m_pos, m_end);

            if (internal::is_error_code(cp)) {
                internal::recover(m_pos, m_end);
                break;
            }
        }

        return *this;
    }

    character_iterator operator++(int)
    {
        auto tmp = *this;
        ++(*this);
        return tmp;
    }
    /// @}


    /** Reverses to the previous character (1..n bytes). */
    /// @{
    character_iterator& operator--()
    {
        // bescbau
        return *this;
    }

    character_iterator operator--(int)
    {
        auto tmp = *this;
        --(*this);
        return tmp;
    }
    /// @}


    friend bool operator==(const character_iterator& lhs, const character_iterator& rhs)
    {
        return lhs.m_byte_pos == rhs.m_byte_pos;
    }

    friend bool operator==(const character_iterator& lhs, const ByteIter& rhs)
    {
        return lhs.m_byte_pos == rhs;
    }

    friend bool operator==(const ByteIter& lhs, const character_iterator& rhs)
    {
        return lhs == rhs.m_byte_pos;
    }

    friend bool operator<(const character_iterator& lhs, const character_iterator& rhs)
    {
        return lhs.m_byte_pos < rhs.m_byte_pos;
    }

    friend bool operator<(const character_iterator& lhs, const ByteIter& rhs)
    {
        return lhs.m_byte_pos < rhs;
    }

    friend bool operator<(const ByteIter& lhs, const character_iterator& rhs)
    {
        return lhs < rhs.m_byte_pos;
    }

private:
    ByteIter m_pos;
    ByteIter m_end;
};

template<typename ByteIter>
bool operator!=(const character_iterator<ByteIter>& lhs, const character_iterator<ByteIter>& rhs)
{
    return not(lhs == rhs);
}

template<typename ByteIter>
bool operator!=(const character_iterator<ByteIter>& lhs, const ByteIter& rhs)
{
    return not(lhs == rhs);
}

template<typename ByteIter>
bool operator!=(const ByteIter& lhs, const character_iterator<ByteIter>& rhs)
{
    return not(lhs == rhs);
}


template<typename ByteIter>
bool operator>(const character_iterator<ByteIter>& lhs, const character_iterator<ByteIter>& rhs)
{
    return rhs < lhs;
}

template<typename ByteIter>
bool operator>(const character_iterator<ByteIter>& lhs, const ByteIter& rhs)
{
    return rhs < lhs;
}

template<typename ByteIter>
bool operator>(const ByteIter& lhs, const character_iterator<ByteIter>& rhs)
{
    return rhs < lhs;
}


template<typename ByteIter>
bool operator>=(const character_iterator<ByteIter>& lhs, const character_iterator<ByteIter>& rhs)
{
    return not(lhs < rhs);
}

template<typename ByteIter>
bool operator>=(const character_iterator<ByteIter>& lhs, const ByteIter& rhs)
{
    return not(lhs < rhs);
}

template<typename ByteIter>
bool operator>=(const ByteIter& lhs, const character_iterator<ByteIter>& rhs)
{
    return not(lhs < rhs);
}


template<typename ByteIter>
bool operator<=(const character_iterator<ByteIter>& lhs, const character_iterator<ByteIter>& rhs)
{
    return not(lhs > rhs);
}

template<typename ByteIter>
bool operator<=(const character_iterator<ByteIter>& lhs, const ByteIter& rhs)
{
    return not(lhs > rhs);
}

template<typename ByteIter>
bool operator<=(const ByteIter& lhs, const character_iterator<ByteIter>& rhs)
{
    return not(lhs > rhs);
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


template<typename ByteIter>
ByteIter skip_bom(ByteIter first, ByteIter past_last)
{
    if (first == past_last || internal::deref8(first) != 0xef) {
        return first;
    }

    if (std::distance(first, past_last) < 3) {
        // text consists of an incomplete BOM only
        return past_last;
    }

    ++first;
    ++first;
    return ++first;
}

template<typename Text>
std::basic_string_view<typename Text::CharT, typename Text::Traits> skip_bom(const Text& t)
{
    const auto past_last = t.end();
    const auto first = skip_bom(t.begin(), past_last);

    const auto dist = std::distance(first, past_last);
    assert(dist >= 0);

    if (dist == 0) {
        return {};
    }

    return {*first, static_cast<typename Text::size_type>(dist)};
}


template<typename ByteIter>
character_quantity character_count(ByteIter first, ByteIter past_last)
{
    const auto dist = std::distance(
            character_iterator(first, past_last), character_iterator(past_last, past_last));
    assert(dist >= 0);
    return character_quantity(static_cast<character_quantity::value_type>(dist));
}

template<typename Text>
character_quantity character_count(const Text& t)
{
    return character_count(t.begin(), t.end());
}


}  // namespace utf8
}  // namespace unicode
