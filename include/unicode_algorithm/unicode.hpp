/* Part of https://gitlab.com/be-sc/unicode_algorithm. Licence: MIT. See LICENSE.rst. */
#pragma once

#include "internal/internal.hpp"
#include <cstddef>
#include <cstdint>

namespace unicode
{

using code_point_quantity = internal::quantity<std::size_t, struct code_point_quantity_tag>;
using character_quantity = internal::quantity<std::size_t, struct character_quantity_tag>;


enum class boundary_class : std::uint8_t
{
    // When adding/removing enumerators, always check ``connects_count`` in the cpp!
    other,
    carriage_return,
    line_feed,
    control,
    prepend,
    extend,
    spacing_mark,
    zero_width_joiner,
    hangul_l,
    hangul_v,
    hangul_t,
    hangul_lv,
    hangul_lvt,
    regional_indicator,
    // When adding/removing enumerators, always check ``connects_count`` in the cpp!
};


struct character_properties
{
    boundary_class boundary_class;
};


class code_point
{
public:
    using value_type = std::uint32_t;

    constexpr code_point() noexcept = default;
    constexpr explicit code_point(value_type value) noexcept : m_value(value) {}

    constexpr value_type get() const noexcept { return m_value; }

    const character_properties& properties() const;

    // clang-format off
    constexpr bool operator==(code_point rhs) const noexcept { return this->m_value == rhs.m_value; }
    constexpr bool operator!=(code_point rhs) const noexcept { return this->m_value != rhs.m_value; }
    constexpr bool operator<(code_point rhs) const noexcept { return this->m_value < rhs.m_value; }
    constexpr bool operator>(code_point rhs) const noexcept { return this->m_value > rhs.m_value; }
    constexpr bool operator<=(code_point rhs) const noexcept { return this->m_value <= rhs.m_value; }
    constexpr bool operator>=(code_point rhs) const noexcept { return this->m_value >= rhs.m_value; }
    // clang-format on

private:
    value_type m_value{0};
};


bool grapheme_continues(code_point lhs, code_point rhs);

}  // namespace unicode
