/* Part of https://gitlab.com/be-sc/unicode_algorithm. Licence: MIT. See LICENSE.rst. */
#pragma once

#include <cstddef>
#include <cstdint>

namespace unicode
{
namespace utf8
{

/** Size in bytes of a UTF-8 code unit (assuming 8-bit bytes). */
constexpr const std::size_t size_of_code_unit = 1;

/** Role of a code unit (one 8-bit byte) in the UTf-8 encoding scheme. */
enum class code_unit_role
{
    invalid,  ///< invalid data (range 0xf8 to 0xff)
    ascii,    ///< standalone code unit coding an ASCII character 00..7F
    head2,    ///< first code unit of a two code unit sequence
    head3,    ///< first code unit of a three code unit sequence
    head4,    ///< first code unit of a four code unit sequence
    tail,     ///< subsequent code unit of a sequence coding a multi-cu code point
};

enum class error_reason : std::uint32_t
{
    // Value of `invalid_data` is smallest, all other enumerator values must be larger than that.
    // All values lie outside of the unicode range (greater than 0x10ffff).
    invalid_data = 0x20ffffff,           ///< code unit has a value not allowed in UTF-8
    incomplete_code_point = 0x21ffffff,  ///< too few tail bytes after a head
    unexpected_tail = 0x22ffffff,        ///< tail byte at unexpected position
    broken_bom = 0x23ffffff,             ///< incomplete/invalid byte order mark (BOM: EF BB BF)
};

}  // namespace utf8
}  // namespace unicode
